import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{


  constructor(private http:HttpClient){}
  ngOnInit(): void {}
    
    onCreatePost(postData: { title: string; content: string }) {
      // Send Http request
      this.http.post('https://mathihttp-default-rtdb.asia-southeast1.firebasedatabase.app/data.json',postData)
      .subscribe(responseData => {
        console.log(responseData);
      });

     }

  }
  

